﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebApplication2.Controllers.Services;

namespace WebApplication2.Controllers
{
    [ApiController]
    
    public class ValuesController : ControllerBase
    {

        public ValuesController() { }

        public static List<Models> data = new List<Models> { new Models { Id = 1, Name = "Ajith", DateOfBirth = DateTime.Parse("1996-11-14"), Gender = "Male", Department = "ECE" }  };

        

        [Route("api/student/list")]
       
        [HttpGet]

        public IActionResult getData()
        {
            return Ok(data);
        }

        [Route("api/student/list/{id}")]

        [HttpGet]

        public IActionResult getSingleData(int id)
        {
            var singleData = data.Single(x => x.Id == id);
            return Ok(singleData);

        }

        [Route("api/student/create")]
        [HttpPost]
        public IActionResult setData([FromBody] Models value)
        {
            data.Add(value);
            return Ok();
        }


        [Route("api/student/update/{id}")]
        [HttpPut]
        public IActionResult put(int id,[FromBody] Models value) {

            var singleData = data.Single(x => x.Id == id);
            singleData.Id = value.Id;
            singleData.Name = value.Name;
            singleData.DateOfBirth = value.DateOfBirth;
            singleData.Gender = value.Gender;
            singleData.Department = value.Department;
            return Ok();
        }

        [Route("api/student/delete/{id}")]
        [HttpDelete]
        public IActionResult delete(int id)
        {

            var singleData = data.Single(x => x.Id == id);
            data.Remove(singleData);
            return Ok();

        }


    }
}
